package com.example.johann.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //OnCreate, ce qui vas se passer au lancement de l'activité
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Chargement de la vue
        setContentView(R.layout.activity_main);
        //Listener du boutton connexion
        findViewById(R.id.connexion).setOnClickListener(this);

        //message qui vas se generer à l'execution de l'activité
        Toast.makeText(this,"Bienvenue",Toast.LENGTH_LONG).show();
    }

    //ce qu'il se passe en cas de click du bouton
    @Override
    public void onClick(View view) {
        //Vas chercher le mot de passe dans activity_main.xml
        final EditText passwordEditText = (EditText) findViewById(R.id.password_main);
        //Extrait sa String
        final String passwordToString = passwordEditText.getText().toString();

        //message qui vas se lance au click du boutton
        Toast.makeText(this,"Bravo !",Toast.LENGTH_LONG).show();

        //si la case mot de passe est vide
        if(passwordToString.equals(""))
            passwordEditText.setError("erreur cette case est vide");


    }
}
